#make -prRq -f Makefile | grep -E "^Modules/\w*\.so" | sed "s|Modules/\(\w*\).so:.*|\\1|"
LIB_DYNLOADS=cStringIO _functoolsmodule errnomodule operator _shamodule zlibmodule threadmodule math parsermodule _socketmodule _elementtree _symtablemodule _struct arraymodule xxsubtype posixmodule cPickle pyexpat unicodedata itertoolsmodule binascii _md5module _weakref _collectionsmodule pwdmodule _randommodule _sha512module datetimemodule _sha256module timemodule _sre _codecsmodule zipimport signalmodule _ssl selectmodule


# Host
host:
	make -f Makefile.in .build/python2.7.2-host/python.exe

# Darwin: MacOS, iPhone
darwin:
	make -f Makefile-darwin .build/python2.7.1-darwin/libpython2.7.so

darwin-x86_64:
	make -f Makefile-darwin .build/python2.7.1-darwin-x86_64/libpython2.7.so

darwin-i386:
	make -f Makefile-darwin .build/python2.7.1-darwin-i386/libpython2.7.so

darwin-arm:
	make -f Makefile-darwin .build/python2.7.1-darwin-arm/libpython2.7.so

# Android
android android-arm:
	@#make -f Makefile-android $(addprefix .build/python2.7.2-android-arm/, libpython2.7.so $(addprefix Modules/, $(addsuffix .so,${LIB_DYNLOADS})) sharedmods)
	make -f Makefile-android $(addprefix .build/python2.7.2-android-arm/, libpython2.7.so $(addprefix Modules/, $(addsuffix .so,${LIB_DYNLOADS})) oldsharedmods sharedmods)

# Linux
linux-i386:
	#make -f Makefile-linux $(addprefix .build/python2.7.2-linux-i386/, libpython2.7.so $(addprefix Modules/, $(addsuffix .so,${LIB_DYNLOADS})) all)
	make -f Makefile-linux-i386 $(addprefix .build/python2.7.2-linux-i386/, libpython2.7.so $(addprefix Modules/, $(addsuffix .so,${LIB_DYNLOADS})) oldsharedmods sharedmods)

linux linux-x86_64:
	#make -f Makefile-linux $(addprefix .build/python2.7.2-linux-x86_64/, libpython2.7.so $(addprefix Modules/, $(addsuffix .so,${LIB_DYNLOADS})) all)
	make -f Makefile-linux-x86_64 $(addprefix .build/python2.7.2-linux-x86_64/, libpython2.7.so $(addprefix Modules/, $(addsuffix .so,${LIB_DYNLOADS})) oldsharedmods sharedmods)
